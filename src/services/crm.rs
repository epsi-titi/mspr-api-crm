use actix_web::{get, HttpResponse, Responder, web::{Data, Path}};
use sqlx::{self};
use crate::{AppState};
use sqlx::{Row};

#[path = "../models/crm_models.rs"]
mod crm_models;
use crm_models::{Order, CustomerDetails, ProductWithDetails, Detail};

#[get("/customers/{customer_id}/{order_id}/products")]
pub async fn fetch_customer_order_products(state: Data<AppState>, path: Path<(i32, i32)>) -> impl Responder {
    let (customer_id, order_id) = path.into_inner();
    match sqlx::query(
        "
            SELECT p.id, p.name, p.created_at, p.stock, d.id AS details_id, d.price, d.description, d.color
            FROM products p
            JOIN details d ON d.id = p.details_id
            JOIN order_products op ON op.product_id = p.id
            JOIN orders o ON o.id = op.id
            WHERE o.id = $1 AND o.customer_id = $2
        "
    )
        .bind(order_id)
        .bind(customer_id)
        .fetch_all(&state.db)
        .await
    {
        Ok(rows) => {
            if rows.is_empty() {
                return HttpResponse::NotFound().json("Les produits n'ont pas été trouvés.");
            }
            let products: Vec<ProductWithDetails> = rows
                .into_iter()
                .map(|row| ProductWithDetails {
                    id: row.get("id"),
                    name: row.get("name"),
                    created_at: row.get("created_at"),
                    stock: row.get("stock"),
                    details: Detail {
                        id: row.get("details_id"),
                        price: row.get("price"),
                        description: row.get("description"),
                        color: row.get("color"),
                    },
                })
                .collect();
            HttpResponse::Ok().json(products)
        }
        Err(_) => HttpResponse::NotFound().json("Les produits n'ont pas été trouvés."),
    }
}

#[get("/customers")]
pub async fn fetch_customers(state: Data<AppState>) -> impl Responder {
    match sqlx::query(
        "
            SELECT c.cu_id, c.name, c.username, c.firstname, c.lastname, c.created_at,
            a.zipcode, a.city,
            cp.name AS company_name,
            p.firstname AS profile_firstname,
            p.lastname AS profile_lastname
            FROM customers c
            JOIN addresses a ON a.id = c.address_id
            JOIN companys cp ON cp.id = c.company_id
            JOIN profiles p ON p.id = c.profile_id
        "
    )
        .fetch_all(&state.db)
        .await
    {
        Ok(rows) => {
            let mut customers_details = Vec::new();
            for row in rows {
                let customer_details = CustomerDetails {
                    cu_id: row.get("cu_id"),
                    name: row.get("name"),
                    username: row.get("username"),
                    firstname: row.get("firstname"),
                    lastname: row.get("lastname"),
                    created_at: row.get("created_at"),
                    zipcode: row.get("zipcode"),
                    city: row.get("city"),
                    company_name: row.get("company_name"),
                    profile_firstname: row.get("profile_firstname"),
                    profile_lastname: row.get("profile_lastname"),
                };
                customers_details.push(customer_details);
            }
            HttpResponse::Ok().json(customers_details)
        }
        Err(_) => HttpResponse::NotFound().json("Les clients n'ont pas été trouvés."),
    }
}


#[get("/customers/{id}/orders")]
pub async fn fetch_customer_orders(state: Data<AppState>, path: Path<i32>) -> impl Responder {
    let customer_id: i32 = path.into_inner();

    match sqlx::query_as::<_, Order>(
        "
            SELECT id, customer_id, created_at
            FROM orders
            WHERE customer_id = $1
        "
    )
        .bind(customer_id)
        .fetch_all(&state.db)
        .await
    {
        Ok(orders) => HttpResponse::Ok().json(orders),
        Err(_) => HttpResponse::NotFound().json("Les commandes pour ce client n'ont pas été trouvées"),
    }
}
