use chrono::{NaiveDate};
use serde::{Serialize};
use sqlx::{self, FromRow, Type};

// TABLES
#[derive(Serialize, FromRow)]
pub struct Addresse {
    pub id: i32,
    pub zipcode: String,
    pub city: String,
}

#[derive(Serialize, FromRow)]
pub struct Company {
    pub id: i32,
    pub name: String,
}

#[derive(Serialize, FromRow, Type)]
pub struct Customer {
    pub cu_id: i32,
    pub name: String,
    pub username: String,
    pub firstname: String,
    pub lastname: String,
    #[serde(skip_serializing)]
    pub created_at: NaiveDate,
    pub address_id: i32,
    pub profile_id: i32,
    pub company_id: i32,
}

#[derive(Serialize, FromRow)]
pub struct Detail {
    pub id: i32,
    pub price: i32,
    pub description: String,
    pub color: String,
}

#[derive(Serialize, FromRow)]
pub struct Order {
    pub id: i32,
    pub customer_id: i32,
    pub created_at: NaiveDate,
}

#[derive(Serialize, FromRow)]
pub struct OrderProduct {
    pub order_id: i32,
    pub product_id: i32,
}

#[derive(Serialize, FromRow)]
pub struct Product {
    pub id: i32,
    pub name: String,
    pub created_at: NaiveDate,
    pub stock: i32,
    pub details_id: i32,
}

#[derive(Serialize, FromRow)]
pub struct Profile {
    pub id: i32,
    pub firstname: String,
    pub lastname: String,
}

// CUSTOM STRUCTS

#[derive(Serialize, FromRow)]
pub struct CustomerDetails {
    pub cu_id: i32,
    pub name: String,
    pub username: String,
    pub firstname: String,
    pub lastname: String,
    pub created_at: NaiveDate,
    pub zipcode: String,
    pub city: String,
    pub company_name: String,
    pub profile_firstname: String,
    pub profile_lastname: String,
}

#[derive(Serialize, FromRow)]
pub struct ProductWithDetails {
    pub id: i32,
    pub name: String,
    pub created_at: NaiveDate,
    pub stock: i32,
    pub details: Detail,
}