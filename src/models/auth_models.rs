use serde::{Serialize, Deserialize};
use sqlx::{self, FromRow};

#[derive(Serialize, Deserialize, Clone)]
pub struct TokenClaims {
    pub id: i32,
}

#[derive(Deserialize)]
pub struct CreateUserBody {
    pub username: String,
    pub password: String,
}

#[derive(Serialize, FromRow)]
pub struct UserNoPassword {
    pub id: i32,
    pub username: String,
}

#[derive(Serialize, FromRow)]
pub struct AuthUser {
    pub id: i32,
    pub username: String,
    pub password: String,
}
//
// sudo apt-get install libclang-dev