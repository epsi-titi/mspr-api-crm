use actix_web::{
    App,
    HttpServer,
    web::{self, Data},
    dev::ServiceRequest,
    error::Error,
    HttpMessage
};
use dotenv::dotenv;
use std::env;
use sqlx::{postgres::PgPoolOptions, Pool, Postgres};

#[path = "services/crm.rs"]
mod crm_services;
use crm_services::{fetch_customer_orders, fetch_customers, fetch_customer_order_products};

#[path = "services/auth.rs"]
mod auth_services;
use auth_services::{create_user, basic_auth};

#[path = "models/crm_models.rs"]
mod crm_models;


#[path = "models/auth_models.rs"]
mod auth_models;
use auth_models::{TokenClaims};


use actix_web_httpauth::{
    extractors::{
        bearer::{self, BearerAuth},
        AuthenticationError,
    },
    middleware::HttpAuthentication,
};
use hmac::{Hmac, Mac};
use jwt::VerifyWithKey;
use sha2::Sha256;

pub struct AppState {
    db: Pool<Postgres>,
}

async fn validator(
    req: ServiceRequest,
    credentials: BearerAuth,
) -> Result<ServiceRequest, (Error, ServiceRequest)> {
    let jwt_secret: String = std::env::var("JWT_SECRET").expect("JWT_SECRET must be set!");
    let key: Hmac<Sha256> = Hmac::new_from_slice(jwt_secret.as_bytes()).unwrap();
    let token_string = credentials.token();

    let claims: Result<TokenClaims, &str> = token_string
        .verify_with_key(&key)
        .map_err(|_| "Invalid token");

    match claims {
        Ok(value) => {
            req.extensions_mut().insert(value);
            Ok(req)
        }
        Err(_) => {
            let config = req
                .app_data::<bearer::Config>()
                .cloned()
                .unwrap_or_default()
                .scope("");

            Err((AuthenticationError::from(config).into(), req))
        }
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    let db_url: String = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&db_url)
        .await
        .expect("Error building a connection pool");


    HttpServer::new(move || {
        let bearer_middleware = HttpAuthentication::bearer(validator);
        App::new()
            .app_data(Data::new(AppState { db: pool.clone() }))
            .service(create_user)
            .service(basic_auth)
            .service(web::scope("")
                         .wrap(bearer_middleware)
                         .service(fetch_customer_orders)
                         .service(fetch_customers)
                         .service(fetch_customer_order_products), )
    })
        .bind(("127.0.0.1", 8080))?
        .run()
        .await
}

